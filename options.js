// Saves options to chrome.storage
function save_options() {
  // var color = document.getElementById('color').value;
  var alerts = document.getElementById('showAlerts').checked;
  var consol = document.getElementById('consoleLog').checked;
  var jqueryLoadLocal = document.getElementById('jQueryLocalOn').checked;
  var jqueryLoadCustomURL = document.getElementById('jQueryCustomURL').value;
  var jqueryLoadMs = document.getElementById('jQueryMs').checked;
  var jqueryLoadUseso = document.getElementById('jQueryUseso').checked;
  var jqueryLoadBootCss = document.getElementById('jQueryBootCss').checked;
  var jqueryLoadCustom = document.getElementById('jQueryCustomOn').checked;
  var operateOnlySE = document.getElementById('onlySE').checked;
  var operateStab = document.getElementById('stubOn').checked;
  var operateAllUrl = document.getElementById('allUrlOn').checked;
  chrome.storage.sync.set({
    // favoriteColor: color,
    alertsOn: alerts,
    consolOn: consol,
    jqueryLocal: jqueryLoadLocal,
    CustomJqURL: jqueryLoadCustomURL,
    jQueryMsOn: jqueryLoadMs,
    jQueryUsesoOn: jqueryLoadUseso,
    jQueryBootCssOn: jqueryLoadBootCss,
    jQueryLoadCustomOn: jqueryLoadCustom,
    operateOnlySeOn: operateOnlySE,
    operateStabOn: operateStab,
    operateAllUrlOn: operateAllUrl
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
// Remember to clean the code a bit after some testing ( loop instead of single )
function restore_options() {
  // Use default value color = 'red' and showAlerts = true.
  chrome.storage.sync.get({
    // favoriteColor: 'red',
    alertsOn: false,
    consolOn: true,
    jqueryLocal: true,
    CustomJqURL: '',
    jQueryMsOn: false,
	jQueryUsesoOn: false,
	jQueryBootCssOn: false,
	jQueryLoadCustomOn: false,
	operateOnlySeOn: true,
	operateStabOn: true,
	operateAllUrlOn: false
  }, function(items) {
    // document.getElementById('color').value = items.favoriteColor;
    document.getElementById('showAlerts').checked = items.alertsOn;
    document.getElementById('consoleLog').checked = items.consolOn;
    document.getElementById('jQueryLocalOn').checked = items.jqueryLocal;
    document.getElementById('jQueryCustomURL').value = items.CustomJqURL;
    document.getElementById('jQueryMs').checked = items.jQueryMsOn;
    document.getElementById('jQueryUseso').checked = items.jQueryUsesoOn;
    document.getElementById('jQueryBootCss').checked = items.jQueryBootCssOn;
    document.getElementById('jQueryCustomOn').checked = items.jQueryLoadCustomOn;
    document.getElementById('onlySE').checked = items.operateOnlySeOn;
    document.getElementById('stubOn').checked = items.operateStabOn;
    document.getElementById('allUrlOn').checked = items.operateAllUrlOn;
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);