// Copyright (c) 2014 Obmerk99
// Licence is absent

// Simple extension to ovveride CDN loading script on bad-coded websites

function getStoragex() {

var keys = ['alertsOn',
					'consolOn',
					'jqueryLocal',
					'CustomJqURL',
					'jQueryMsOn',
					'jQueryUsesoOn',
					'jQueryBootCssOn',
					'jQueryLoadCustomOn',
					'operateOnlySeOn',
					'operateStabOn',
					'operateAllUrlOn'];
var optionShowAlertsx = {};

chrome.storage.sync.get(keys, function(result) {

	keys.forEach(function(key){
        // alert(result[key]);
		// optionShowAlertsxY[key] = result[key];
		optionShowAlertsx[key] = result[key];
    });

	  // console.info("DATA " + optionShowAlertsx);
	 returnMe = optionShowAlertsx;
	  
    });
	  return returnMe;
}


chrome.webRequest.onBeforeRequest.addListener(
  function(info) {
	
	// Not usefull method, works for me now ( and only me ) will update soon.
	var cdnOriginalUrl = 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
	var cdnOriginal = 'googleapis';
	var cdnReplace = 'useso';
	var cdnLoad = cdnOriginalUrl.replace(cdnOriginal, cdnReplace); 
	
	// Get the options
	var optionsNow = getStoragex();
	
	if ( optionsNow.consolOn === true ){
		console.info("Show Alerts is  " + optionsNow.alertsOn);
		console.info("Console Log is " +  optionsNow.consolOn);
		console.info("jQuery Local is  " +  optionsNow.jqueryLocal);
		console.info("jQuery Custom URL " + optionsNow.CustomJqURL);
		console.info("jQuery Ms is " +  optionsNow.jQueryMsOn);
		console.info("jQuery Useso is " +  optionsNow.jQueryUsesoOn);
		console.info("jQuery BootCss is " +  optionsNow.jQueryBootCssOn);
		console.info("jQuery LoadCustom is " +  optionsNow.jQueryLoadCustomOn);
		console.info("Operating on SE is " +  optionsNow.operateOnlySeOn);
		console.info("Operating on stab file is " +  optionsNow.operateStabOn);
		console.info("Operating on all URLs is " +  optionsNow.operateAllUrlOn);
	}
	
	// clean if needed a single key
	
	// chrome.storage.sync.remove("showAlert", function(data) {
      // console.log("removed", data);
    // });

	console.info("Found Script on page " + info.url);
	
	
	
	var stubJs = chrome.extension.getURL("js/stub.en.js");
	// var jqueryURL = chrome.extension.getURL("js/jquery.min.js");
	
	if ( optionsNow.jqueryLocal === true ){
		var jqueryURL = chrome.extension.getURL("js/jquery.min.js");
	} else if ( optionsNow.jQueryMsOn === true ){
		var jqueryURL = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js";
	} else if ( optionsNow.jQueryUsesoOn === true ){
		var jqueryURL = "http://ajax.useso.com/ajax/libs/jquery/1.7.1/jquery.min.js'";
	} else if ( optionsNow.jQueryBootCssOn === true ){
		var jqueryURL = "http://cdn.bootcss.com/jquery/1.7.1/jquery.min.js";
	} else if ( optionsNow.jQueryLoadCustomOn === true ){
		var jqueryURL = CustomJqURL;
	} else {
		var jqueryURL = cdnLoad;
	}
	
	
	
	// if (info.url==="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"){
	 if (info.url.indexOf("jquery.min.js") > -1) {
	 
		if ( optionsNow.consolOn === true ){
			console.info("REDIRECTING --> " + info.url + " is being redirected to " + jqueryURL);
		}
		if ( optionsNow.alertsOn === true ){
			alert("URL was redirected from <<-- " + info.url + " to -->> " + jqueryURL);
		}
		return {redirectUrl: jqueryURL};
		
	// } else if (info.url==="http://cdn.sstatic.net/Js/stub.en.js?v=0368210018a1") {
	
	} else if (info.url.indexOf("stub.en.js?v") > -1) {
	
		if ( optionsNow.consolOn === true ){
			console.info("REDIRECTING --> " + info.url + " is being redirected to " + stubJs);
		}
		if ( optionsNow.alertsOn === true ){
			alert("URL was redirected from <<-- " + info.url + " to -->> " + stubJs);
		}
		return {redirectUrl:  stubJs};
	} else {
		return {redirectUrl:  info.url};
	}

  },
  // filters Comment
  {
    urls: [
			"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js",
			"http://cdn.sstatic.net/Js/stub.en.js*",
			"http://*.stackoverflow.com/*"
    ],
    types: ["script"]
  },
  // extraInfoSpec
  ["blocking"]);
